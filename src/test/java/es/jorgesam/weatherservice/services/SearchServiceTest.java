package es.jorgesam.weatherservice.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import es.jorgesam.weatherservice.core.actions.weather.GetWeatherSearchesAction;
import es.jorgesam.weatherservice.core.actions.weather.SearchWeatherAction;
import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.services.dto.WeatherAndLastSearchesDTO;

@ExtendWith(MockitoExtension.class)
class SearchServiceTest {

    private Random random = new Random();

    private SearchService searchService;
    @Mock
    private SearchWeatherAction searchWeather;
    @Mock
    private GetWeatherSearchesAction getWeatherSearches;

    @BeforeEach
    private void init() {
        searchService = new SearchServiceImpl(searchWeather, getWeatherSearches);
    }

    @Test
    void checkActionsAreUsedWhenNulls() throws DataAccessException {
        searchService.search(null, null, null);
        Mockito.verify(searchWeather).findBy(null, null, null);
        Mockito.verify(getWeatherSearches).findTop10ByUser(null);
    }

    @Test
    void checkActionsAreUsedWhenEmptyQueryString() throws DataAccessException {
        SearchArgs args = SearchArgs.emptySearch(random);
        searchService.search(args.query, args.username, args.instant);
        Mockito.verify(searchWeather).findBy(args.query, args.username, args.instant);
        Mockito.verify(getWeatherSearches).findTop10ByUser(args.username);
    }

    @RepeatedTest(10)
    void checkArgumentsArrivesToActions() throws DataAccessException {
        SearchArgs args = SearchArgs.randomSearch(random);
        searchService.search(args.query, args.username, args.instant);
        Mockito.verify(searchWeather).findBy(args.query, args.username, args.instant);
        Mockito.verify(getWeatherSearches).findTop10ByUser(args.username);
    }

    @Test
    void searchWithNoResults() throws DataAccessException {
        SearchArgs args = SearchArgs.randomSearch(random);
        when(searchWeather.findBy(args.query, args.username, args.instant)).thenReturn(Optional.empty());
        when(getWeatherSearches.findTop10ByUser(args.username)).thenReturn(new ArrayList<>());
        WeatherAndLastSearchesDTO results = searchService.search(args.query, args.username, args.instant);
        assertNotNull(results);
        assertTrue(results.getWeather().isEmpty());
        assertTrue(results.getLastSearches().isEmpty());
    }

    @Test
    void searchWithResults() throws DataAccessException {
        SearchArgs args = SearchArgs.randomSearch(random);
        when(searchWeather.findBy(args.query, args.username, args.instant)).thenReturn(Optional.of(new Weather()));
        when(getWeatherSearches.findTop10ByUser(args.username)).thenReturn(new ArrayList<>());
        WeatherAndLastSearchesDTO results = searchService.search(args.query, args.username, args.instant);
        assertTrue(results.getWeather().isPresent());
    }

    @Test
    void searchWithLastSearches() throws DataAccessException {
        SearchArgs args = SearchArgs.randomSearch(random);
        List<WeatherSearch> weatherSearchsList = new ArrayList<>();
        for (int i = 0; i < random.nextInt(10) + 1; i++) {
            weatherSearchsList.add(new WeatherSearch());
        }
        when(searchWeather.findBy(args.query, args.username, args.instant)).thenReturn(Optional.empty());
        when(getWeatherSearches.findTop10ByUser(args.username)).thenReturn(weatherSearchsList);
        WeatherAndLastSearchesDTO results = searchService.search(args.query, args.username, args.instant);
        assertFalse(results.getLastSearches().isEmpty());
        assertEquals(weatherSearchsList.size(), results.getLastSearches().size());
    }

    private static class SearchArgs {
        public String query;
        public String username;
        public Instant instant;

        public static SearchArgs randomSearch(Random random) {
            SearchArgs args = new SearchArgs();
            args.query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
            args.username = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
            args.instant = Instant.now();
            return args;
        }

        public static SearchArgs emptySearch(Random random) {
            SearchArgs args = new SearchArgs();
            args.query = "";
            args.username = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
            args.instant = Instant.now();
            return args;
        }
    }
}
