package es.jorgesam.weatherservice.util.geonames.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherObservation;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNameDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.ObservationDTO;

class GeoNamesResultConverterTest {

    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private Random random = new Random();
    private GeoNamesResultConverter converter;

    @BeforeEach
    void init() {
        converter = new GeoNamesResultConverter();
        converter.setDatePattern(DATE_PATTERN);
    }

    @Test
    void optionalEmptyWhenNoValues() {
        assertTrue(converter.convert(null, null).isEmpty());
        assertTrue(converter.convert(new GeoNamesSearchResponseDTO(), null).isEmpty());

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        assertTrue(converter.convert(new GeoNamesSearchResponseDTO(), weatherResponse).isEmpty());

        Collection<ObservationDTO> observationsList = new ArrayList<>();
        weatherResponse.setWeatherObservations(observationsList);
        assertTrue(converter.convert(new GeoNamesSearchResponseDTO(), weatherResponse).isEmpty());
    }

    @Test
    void optionalPresentWhenValues() {

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        Collection<ObservationDTO> observationsList = new ArrayList<>();
        ObservationDTO observation = new ObservationDTO();
        observationsList.add(observation);
        weatherResponse.setWeatherObservations(observationsList);

        assertTrue(converter.convert(new GeoNamesSearchResponseDTO(), weatherResponse).isPresent());
    }

    @Test
    void checkValues() {

        GeoNameDTO geoName = new GeoNameDTO();
        geoName.setName(RandomStringUtils.randomAlphabetic(random.nextInt(10)));
        geoName.setLat(String.valueOf(random.nextDouble()));
        geoName.setLng(String.valueOf(random.nextDouble()));

        List<GeoNameDTO> geoNamesList = new ArrayList<>();
        geoNamesList.add(geoName);

        GeoNamesSearchResponseDTO searchResponse = new GeoNamesSearchResponseDTO();
        searchResponse.setTotalResultsCount(1);
        searchResponse.setGeonames(geoNamesList);

        Collection<ObservationDTO> observationsList = new ArrayList<>();
        ObservationDTO observation = new ObservationDTO();
        observation.setLat(random.nextDouble());
        observation.setLng(random.nextDouble());
        observation.setStationName(RandomStringUtils.randomAlphabetic(random.nextInt(10)));
        observation.setHumidity(random.nextInt());
        observation.setTemperature(String.valueOf(random.nextFloat()));
        observationsList.add(observation);

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        weatherResponse.setWeatherObservations(observationsList);

        Optional<Weather> weather = converter.convert(searchResponse, weatherResponse);

        assertEquals(geoName.getName(), weather.get().getName());
        assertEquals(Double.valueOf(geoName.getLat()), weather.get().getLat());
        assertEquals(Double.valueOf(geoName.getLng()), weather.get().getLng());
        assertNotNull(weather.get().getObservations());
        assertEquals(observationsList.size(), weather.get().getObservations().size());

        WeatherObservation weatherObservation = weather.get().getObservations().iterator().next();
        assertEquals(observation.getLat(), weatherObservation.getLat());
        assertEquals(observation.getLng(), weatherObservation.getLng());
        assertEquals(observation.getStationName(), weatherObservation.getStationName());
        assertEquals(Float.valueOf(observation.getTemperature()), weatherObservation.getTemperature());
    }

    @Test
    void checkTemperaturaEmpty() {

        GeoNameDTO geoName = new GeoNameDTO();

        List<GeoNameDTO> geoNamesList = new ArrayList<>();
        geoNamesList.add(geoName);

        GeoNamesSearchResponseDTO searchResponse = new GeoNamesSearchResponseDTO();
        searchResponse.setTotalResultsCount(1);
        searchResponse.setGeonames(geoNamesList);

        Collection<ObservationDTO> observationsList = new ArrayList<>();
        ObservationDTO observation = new ObservationDTO();
        observation.setTemperature("");
        observationsList.add(observation);

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        weatherResponse.setWeatherObservations(observationsList);

        Optional<Weather> weather = converter.convert(searchResponse, weatherResponse);

        assertNotNull(weather.get().getObservations());
        assertEquals(observationsList.size(), weather.get().getObservations().size());

        WeatherObservation weatherObservation = weather.get().getObservations().iterator().next();
        assertNull(weatherObservation.getTemperature());
    }

    @Test
    void checkNullTemperatureWhenNoValidValues() {

        GeoNameDTO geoName = new GeoNameDTO();
        List<GeoNameDTO> geoNamesList = new ArrayList<>();
        geoNamesList.add(geoName);
        GeoNamesSearchResponseDTO searchResponse = new GeoNamesSearchResponseDTO();
        searchResponse.setGeonames(geoNamesList);

        Collection<ObservationDTO> observationsList = new ArrayList<>();
        ObservationDTO observation = new ObservationDTO();
        observation.setTemperature("");
        observationsList.add(observation);

        ObservationDTO observation2 = new ObservationDTO();
        observation2.setTemperature(null);
        observationsList.add(observation2);

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        weatherResponse.setWeatherObservations(observationsList);

        Optional<Weather> weather = converter.convert(searchResponse, weatherResponse);

        assertNull(weather.get().getTemperatureAvg());
    }

    @Test
    void checkAvgTemperatureWhenValidValues() {

        GeoNameDTO geoName = new GeoNameDTO();
        List<GeoNameDTO> geoNamesList = new ArrayList<>();
        geoNamesList.add(geoName);
        GeoNamesSearchResponseDTO searchResponse = new GeoNamesSearchResponseDTO();
        searchResponse.setGeonames(geoNamesList);
        
        float float1 = random.nextFloat();
        float float2 = random.nextFloat();

        Collection<ObservationDTO> observationsList = new ArrayList<>();
        ObservationDTO observation = new ObservationDTO();
        observation.setTemperature("");
        observationsList.add(observation);

        ObservationDTO observation2 = new ObservationDTO();
        observation2.setTemperature(String.valueOf(float1));
        observationsList.add(observation2);
        
        ObservationDTO observation3 = new ObservationDTO();
        observation3.setTemperature(String.valueOf(float2));
        observationsList.add(observation3);

        GeoNamesWeatherResponseDTO weatherResponse = new GeoNamesWeatherResponseDTO();
        weatherResponse.setWeatherObservations(observationsList);

        Optional<Weather> weather = converter.convert(searchResponse, weatherResponse);

        assertEquals( (float1+float2)/2f, weather.get().getTemperatureAvg().floatValue() );
    }

    @Test
    void dateConversion() {
        LocalDateTime date = converter.parseFromStringOrNull("2019-01-05 16:01:02");
        assertNotNull(date);
        assertEquals(2019, date.getYear());
        assertEquals(Month.JANUARY, date.getMonth());
        assertEquals(5, date.getDayOfMonth());
        assertEquals(16, date.getHour());
        assertEquals(1, date.getMinute());
        assertEquals(2, date.getSecond());
    }

}
