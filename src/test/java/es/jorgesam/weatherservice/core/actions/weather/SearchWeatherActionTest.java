package es.jorgesam.weatherservice.core.actions.weather;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherRepository;
import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.exceptions.DataAccessException;

@ExtendWith(MockitoExtension.class)
class SearchWeatherActionTest {

    private static final String FAKE_USER = "fake-user";

    private Random random = new Random();

    private SearchWeatherAction action;
    private Instant instant;

    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private SaveWeatherSearchAction saveWeatherSearchAction;

    @BeforeEach
    private void init() {
        action = new SearchWeatherAction(weatherRepository, saveWeatherSearchAction);
        instant = Instant.now();
    }

    @Test
    void exceptionWhenNoUser() {
        assertThrows(IllegalArgumentException.class, () -> {
            action.findBy(null, null, instant);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            action.findBy(null, "", instant);
        });
    }

    @Test
    void exceptionWhenNoTimestamp() {
        assertThrows(IllegalArgumentException.class, () -> {
            action.findBy(null, FAKE_USER, null);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            action.findBy(null, FAKE_USER, null);
        });
    }

    @Test
    void optionalEmptyWhenNoQuery() throws DataAccessException {
        assertEquals(Optional.empty(), action.findBy(null, FAKE_USER, instant));
    }

    @Test
    void optionalEmptyWhenQueryEmpty() throws DataAccessException {
        assertEquals(Optional.empty(), action.findBy("", FAKE_USER, instant));
    }

    @Test
    void weatherRepositoryIsCalledWithTheSameQuery() throws DataAccessException {
        String query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
        action.findBy(query, FAKE_USER, instant);
        verify(weatherRepository).findBy(query);
    }

    @Test
    void optionalEmptyWhenRepositoryReturnsEmpty() throws DataAccessException {
        String query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
        when(weatherRepository.findBy(query)).thenReturn(Optional.empty());
        assertTrue(action.findBy(query, FAKE_USER, instant).isEmpty());
    }

    @Test
    void optionalNotEmptyWhenRepositoryReturnsElement() throws DataAccessException {
        String query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
        when(weatherRepository.findBy(query)).thenReturn(Optional.of(new Weather()));
        assertTrue(action.findBy(query, FAKE_USER, instant).isPresent());
    }

    @Test
    void searchIsNotSavedWhenNotResults() throws DataAccessException {
        String query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
        when(weatherRepository.findBy(query)).thenReturn(Optional.empty());
        assertTrue(action.findBy(query, FAKE_USER, instant).isEmpty());
        verifyZeroInteractions(saveWeatherSearchAction);
    }

    @Test
    void searchIsSavedWhenResults() throws DataAccessException {
        String query = RandomStringUtils.randomAlphanumeric(random.nextInt(10));
        Weather weatherFound = new Weather();
        when(weatherRepository.findBy(query)).thenReturn(Optional.of(weatherFound));
        assertTrue(action.findBy(query, FAKE_USER, instant).isPresent());
        ArgumentCaptor<WeatherSearch> argument = ArgumentCaptor.forClass(WeatherSearch.class);
        verify(saveWeatherSearchAction).save(argument.capture());
        assertNotNull(argument.getValue());
        // Check right values are saved
        assertEquals(query, argument.getValue().getQuery());
        assertEquals(FAKE_USER, argument.getValue().getUsername());
        assertEquals(instant, argument.getValue().getTimestamp());
        assertEquals(weatherFound.getName(), argument.getValue().getName());
    }
}
