package es.jorgesam.weatherservice.core.actions.weather;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import es.jorgesam.weatherservice.core.domain.WeatherSearchRepository;

@ExtendWith(MockitoExtension.class)
class GetWeatherSearchesActionTest {

    private GetWeatherSearchesAction action;

    @Mock
    private WeatherSearchRepository repository;

    @BeforeEach
    private void init() {
        action = new GetWeatherSearchesAction(repository);
    }

    @Test
    void emptyListIfNotResults() {
        String username = "dummyUser";
        when(repository.findTop10ByUsernameOrderByIdDesc(username))
            .thenReturn(new ArrayList<>());
        assertNotNull(action.findTop10ByUser(username));
        verify(repository).findTop10ByUsernameOrderByIdDesc(username);
    }

    @Test
    void emptyListIfUserIsNull() {
        assertNotNull(action.findTop10ByUser(null));
        verifyZeroInteractions(repository);
    }

    @Test
    void emptyListIfUserIsEmpty() {
        assertNotNull(action.findTop10ByUser(""));
        verifyZeroInteractions(repository);

        assertNotNull(action.findTop10ByUser("  "));
        verifyZeroInteractions(repository);
    }
}
