package es.jorgesam.weatherservice.core.domain;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.util.geonames.client.GeoNamesClient;
import es.jorgesam.weatherservice.util.geonames.converters.GeoNamesResultConverter;
import es.jorgesam.weatherservice.util.geonames.dtos.BBoxDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNameDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.ObservationDTO;

@ExtendWith(MockitoExtension.class)
class WeatherRepositoryTest {

    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private Random random = new Random();
//    private GeoNamesConfig config;
    private WeatherRepository repository;

    @Mock
    private GeoNamesClient geoNamesClient;
    private GeoNamesResultConverter converter;

    @BeforeEach
    private void init() {
        converter = new GeoNamesResultConverter();
        converter.setDatePattern(DATE_PATTERN);
        repository = new WeatherRepositoryImpl(geoNamesClient, converter);
    }

    @Test
    void templateIsNotCalledWhenNullQuery() throws DataAccessException {
        assertTrue(repository.findBy(null).isEmpty());
        verifyZeroInteractions(geoNamesClient);
    }

    @Test
    void templateIsNotCalledWhenEmptyQuery() throws DataAccessException {
        String query = RandomStringUtils.random(10, ' ');
        assertTrue(repository.findBy(query).isEmpty());
        verifyZeroInteractions(geoNamesClient);
    }

    @Test
    void emptyObjectWhenNoResults() throws DataAccessException {
        String query = RandomStringUtils.randomAlphabetic(10);
        GeoNamesSearchResponseDTO dto = new GeoNamesSearchResponseDTO();
        dto.setTotalResultsCount(0);
        dto.setGeonames(new ArrayList<>());

        when(geoNamesClient.searchGeoNames(ArgumentMatchers.any()))
            .thenReturn(dto);

        assertTrue(repository.findBy(query).isEmpty());
    }

    @Test
    void emptyObjectWhenNoObservations() throws DataAccessException {
        String query = RandomStringUtils.randomAlphabetic(10);
        GeoNamesSearchResponseDTO searchDTO = new GeoNamesSearchResponseDTO();
        searchDTO.setTotalResultsCount(2);

        GeoNameDTO firstGeoName = new GeoNameDTO();
        firstGeoName.setName(RandomStringUtils.randomAlphabetic(10));
        firstGeoName.setLat(RandomStringUtils.randomAlphabetic(10));
        firstGeoName.setLng(RandomStringUtils.randomAlphabetic(10));
        firstGeoName.setBbox(new BBoxDTO(
                random.nextDouble(),
                random.nextDouble(),
                random.nextDouble(),
                random.nextDouble()));
        List<GeoNameDTO> geoNames = new ArrayList<>();
        geoNames.add(firstGeoName);
        searchDTO.setGeonames(geoNames);

        GeoNamesWeatherResponseDTO weatherDTO = new GeoNamesWeatherResponseDTO();
        weatherDTO.setWeatherObservations(new ArrayList<>());

        when(geoNamesClient.searchGeoNames(ArgumentMatchers.any())).thenReturn(searchDTO);
        when(geoNamesClient.searchObservations(ArgumentMatchers.any())).thenReturn(weatherDTO);

        assertTrue(repository.findBy(query).isEmpty());
    }

    @Test
    void weatherObjectWhenResults() throws DataAccessException {
        String query = RandomStringUtils.randomAlphabetic(10);
        GeoNamesSearchResponseDTO searchDTO = new GeoNamesSearchResponseDTO();
        searchDTO.setTotalResultsCount(2);

        GeoNameDTO firstGeoName = new GeoNameDTO();
        firstGeoName.setName(RandomStringUtils.randomAlphabetic(10));
        firstGeoName.setLat(String.valueOf(random.nextDouble()));
        firstGeoName.setLng(String.valueOf(random.nextDouble()));
        firstGeoName.setBbox(new BBoxDTO(
                random.nextDouble(),
                random.nextDouble(),
                random.nextDouble(),
                random.nextDouble()));
        List<GeoNameDTO> geoNames = new ArrayList<>();
        geoNames.add(firstGeoName);
        searchDTO.setGeonames(geoNames);

        GeoNamesWeatherResponseDTO weatherDTO = new GeoNamesWeatherResponseDTO();
        List<ObservationDTO> observations = new ArrayList<>();
        ObservationDTO observation1 = new ObservationDTO();
        observation1.setLat(random.nextDouble());
        observation1.setLng(random.nextDouble());
        observation1.setStationName("dummyStationName");
        observation1.setTemperature("20.3");
        observations.add(observation1);

        weatherDTO.setWeatherObservations(observations);

        when(geoNamesClient.searchGeoNames(ArgumentMatchers.any())).thenReturn(searchDTO);
        when(geoNamesClient.searchObservations(ArgumentMatchers.any())).thenReturn(weatherDTO);

        Optional<Weather> weather = repository.findBy(query);
        assertFalse(weather.isEmpty());

    }
}
