package es.jorgesam.weatherservice.testutils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherObservation;

public class RandomModelBuilder {

    private static final Random random = new Random();

    public static Weather buildWeatherModel(int nObservations) {
        Weather model = new Weather();
        model.setName(RandomStringUtils.randomAlphanumeric(random.nextInt(10)));
        model.setLat(random.nextDouble());
        model.setLng(random.nextDouble());
        Collection<WeatherObservation> observations = new ArrayList<>();
        for (int i = 0; i < nObservations; i++) {
            WeatherObservation o = new WeatherObservation();
            o.setDateTime(LocalDateTime.now());
            o.setLat(random.nextDouble());
            o.setLat(random.nextDouble());
            o.setStationName(RandomStringUtils.randomAlphanumeric(random.nextInt(10)));
            // Algunas estaciones sin temperatura
            if (random.nextInt(10) > 3) {
                o.setTemperature(random.nextFloat());
            }
            observations.add(o);
        }
        model.setObservations(observations);
        return model;
    }
}
