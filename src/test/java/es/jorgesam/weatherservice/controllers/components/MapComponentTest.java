package es.jorgesam.weatherservice.controllers.components;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;

import es.jorgesam.weatherservice.services.dto.WeatherDTO;
import es.jorgesam.weatherservice.testutils.RandomModelBuilder;

class MapComponentTest {

    private Random random = new Random();

    @Test
    void componentHasTheRightAttributes() {
        WeatherDTO dto = WeatherDTO.from(RandomModelBuilder.buildWeatherModel(random.nextInt(10)));
        MapComponent component = MapComponent.fromWeatherDTO(dto);
        assertEquals(dto.getName(), component.getLocationName());
        assertEquals(dto.getLat(), component.getLatitude());
        assertEquals(dto.getLng(), component.getLongitude());
        assertEquals(dto.getObservations().size(), component.getMarkers().size());
    }

}
