package es.jorgesam.weatherservice.controllers.components;

import static java.math.RoundingMode.HALF_EVEN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Random;

import org.junit.jupiter.api.Test;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.services.dto.WeatherDTO;
import es.jorgesam.weatherservice.testutils.RandomModelBuilder;

class WeatherInfoComponentTest {

    private Random random = new Random();

    @Test
    void checkNoResultsWhenOptinalIsEmpty() {
        Optional<WeatherDTO> dto = Optional.empty();

        WeatherInfoComponent component = WeatherInfoComponent.fromWeatherDTO(dto);
        assertFalse(component.hasData());
    }

    @Test
    void checkResultsWhenOptinalIsNotEmpty() {
        WeatherDTO dto = new WeatherDTO();
        Optional<WeatherDTO> opt = Optional.of(dto);

        WeatherInfoComponent component = WeatherInfoComponent.fromWeatherDTO(opt);
        assertTrue(component.hasData());
    }

    @Test
    void componentHasTheRightAttributes() {
        Weather model = RandomModelBuilder.buildWeatherModel(random.nextInt(10));
        WeatherDTO dto = WeatherDTO.from(model);
        Optional<WeatherDTO> opt = Optional.of(dto);

        WeatherInfoComponent component = WeatherInfoComponent.fromWeatherDTO(opt);
        assertTrue(component.hasData());
        assertEquals(model.getName(), component.getName());
        assertEquals(roundFloat(model.getTemperatureAvg()), component.getTemperature());
        assertNotNull(component.getMap());
    }

    private Float roundFloat(Float origin) {
        if (origin != null) {
            BigDecimal bd = BigDecimal.valueOf(origin);
            return bd.setScale(1, HALF_EVEN).floatValue();
        } else {
            return null;
        }
    }
}
