package es.jorgesam.weatherservice.controllers.components;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;

import org.junit.jupiter.api.Test;

import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.services.dto.WeatherSearchDTO;

class WeatherSearchComponentTest {

    @Test
    void componentHasTheRightAttributes() {
        String name = "nameToVerify";
        WeatherSearchDTO dto = WeatherSearchDTO.from(new WeatherSearch("anyQuery", "dummyUser", Instant.now(), name));
        WeatherSearchComponent component = WeatherSearchComponent.fromWeatherSearchDTO(dto);
        assertEquals(name, component.getName());
        assertEquals(name.trim().toLowerCase(), component.getQueryString());
    }

}
