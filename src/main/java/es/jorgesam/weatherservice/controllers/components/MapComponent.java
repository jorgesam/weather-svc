package es.jorgesam.weatherservice.controllers.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import es.jorgesam.weatherservice.services.dto.WeatherDTO;

/**
 * Componente de Mapa que encapsula los datos de localización principal y
 * la lista de marcadores a representar.
 */
public class MapComponent {

    private MapPointInfo location;
    private Collection<MapPointInfo> markers;

    public static MapComponent fromWeatherDTO(WeatherDTO dto) {
        MapComponent component = new MapComponent();
        component.location = new MapPointInfo(dto.getName(), dto.getLng(), dto.getLat());
        if (dto.getObservations() != null) {
            component.markers = dto.getObservations().stream()
                    .map(o -> new MapPointInfo(o.getStationName(), o.getLng(), o.getLat()))
                    .collect(Collectors.toList());
        } else {
            component.markers = new ArrayList<>();
        }
        return component;
    }

    public String getLocationName() {
        return location.getLocationName();
    }

    public Double getLatitude() {
        return location.getLatitude();
    }

    public Double getLongitude() {
        return location.getLongitude();
    }

    public Collection<MapPointInfo> getMarkers() {
        return markers;
    }

}
