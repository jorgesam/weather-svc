package es.jorgesam.weatherservice.controllers.components;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import es.jorgesam.weatherservice.services.dto.WeatherDTO;

/**
 * Componente que encapsula la lógica de vista necesaria para mostrar
 * el componente weather-info.html
 */
public class WeatherInfoComponent {

    private static final String BLUE = "#3366ff";
    private static final String ORANGE = "#ffa500";
    private static final String RED = "#ff3333";

    private boolean hasData = false;
    private String name;
    private String countryCode;
    private String timezoneId;
    private Float temperature;
    private String temperatureColor;
    private int percentage;
    private MapComponent map;

    public static WeatherInfoComponent fromWeatherDTO(Optional<WeatherDTO> dto) {
        WeatherInfoComponent component = new WeatherInfoComponent();
        if (dto.isPresent()) {
            component.hasData = true;
            WeatherDTO weatherDTO = dto.get();
            component.name = weatherDTO.getName();
            component.countryCode = weatherDTO.getCountryCode();
            component.timezoneId = weatherDTO.getTimezoneId();
            component.temperature = roundTemperature(weatherDTO.getTemperature());
            component.map = MapComponent.fromWeatherDTO(weatherDTO);
            if (component.temperature != null) {
                if (component.temperature.floatValue() < 15f) {
                    component.temperatureColor = BLUE;
                } else if (component.temperature.floatValue() < 25f) {
                    component.temperatureColor = ORANGE;
                } else {
                    component.temperatureColor = RED;
                }
            }
            if (component.temperature == null || component.temperature < -50f) {
                component.percentage = 0;
            } else if (component.temperature > 50) {
                component.percentage = 50;
            } else {
                component.percentage = 50 + Math.round(component.temperature);
            }
        }
        return component;
    }

    private static Float roundTemperature(Float temperature2) {
        if (temperature2 != null) {
            BigDecimal bd = BigDecimal.valueOf(temperature2);
            return bd.setScale(1, RoundingMode.HALF_EVEN).floatValue();
        } else {
            return null;
        }
    }

    public boolean hasData() {
        return hasData;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public Float getTemperature() {
        return temperature;
    }

    public String getTemperatureColor() {
        return temperatureColor;
    }

    public int getPercentage() {
        return percentage;
    }

    public MapComponent getMap() {
        return map;
    }

}
