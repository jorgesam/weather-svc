package es.jorgesam.weatherservice.controllers.components;

import es.jorgesam.weatherservice.services.dto.WeatherSearchDTO;

/**
 * Lógica del componente weather-search-list.html
 *
 */
public class WeatherSearchComponent {

    private String name;

    public static WeatherSearchComponent fromWeatherSearchDTO(WeatherSearchDTO dto) {
        WeatherSearchComponent component = new WeatherSearchComponent();
        component.name = dto.getName();
        return component;
    }

    public String getName() {
        return name;
    }

    public String getQueryString() {
        return name.trim().toLowerCase();
    }

}
