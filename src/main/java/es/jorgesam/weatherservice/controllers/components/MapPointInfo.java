package es.jorgesam.weatherservice.controllers.components;

/**
 * Punto a representar dentro del componente mapa
 *
 */
public class MapPointInfo {

    private String locationName;
    private Double longitude;
    private Double latitude;

    public MapPointInfo(String locationName, Double longitude, Double latitude) {
        super();
        this.locationName = locationName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    @Override
    public String toString() {
        return "MapPointInfo [locationName=" + locationName + ", longitude=" + longitude + ", latitude=" + latitude
                + "]";
    }

}
