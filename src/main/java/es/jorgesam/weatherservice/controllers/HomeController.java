package es.jorgesam.weatherservice.controllers;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import es.jorgesam.weatherservice.controllers.commands.SearchCommand;
import es.jorgesam.weatherservice.controllers.components.WeatherInfoComponent;
import es.jorgesam.weatherservice.controllers.components.WeatherSearchComponent;
import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.services.SearchService;
import es.jorgesam.weatherservice.services.dto.WeatherAndLastSearchesDTO;

@Controller
@RequestMapping("/")
public class HomeController {

    private static final String SEARCH_COMMAND_ATTRIBUTE = "searchCommand";
    private static final String LAST_SEARCHES_ATTRIBUTE = "lastSearches";
    private static final String WEATHER_INFO_ATTRIBUTE = "weatherInfo";
    private static final String EXISTS_QUERY_ATTRIBUTE = "existsQuery";
    private static final String EXCEPTION_INFO = "exceptionInfo";

    private SearchService searchService;

    public HomeController(@Autowired SearchService searchService) {
        this.searchService = searchService;
    }

    /*
     * Aceptamos peticiones GET al recurso "/". El searchCommand recoge el parámetro query
     * y se usa como objeto backend del formulario, de forma que tenemos los datos de la búsquda
     * realizada al cargar la página (y se muestran en el campo del formulario)
     * 
     * El objeto Authentication es inyectado en cada request automáticamente, lo usamos para obtener el
     * nombre del usuario. Se podría usar también el Principal o acceder directamente a la request, ya
     * que se inyectan también automáticamente
     */
    @GetMapping
    public ModelAndView home(SearchCommand searchCommand, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject(SEARCH_COMMAND_ATTRIBUTE, searchCommand);
        try {
            setComponentsOnModel(
                    searchService.search(searchCommand.getQuery(), authentication.getName(), Instant.now()),
                    modelAndView);
            if (searchCommand.getQuery() != null && searchCommand.getQuery().length() > 0) {
                modelAndView.addObject(EXISTS_QUERY_ATTRIBUTE, true);
            }
        } catch (DataAccessException e) {
            modelAndView.addObject(EXCEPTION_INFO, e);
        }
        return modelAndView;
    }

    /*
     * Convertimos los DTOs que nos ofrece el servicio en componentes reutilizables en la vista
     */
    private void setComponentsOnModel(WeatherAndLastSearchesDTO dto, ModelAndView model) {
        List<WeatherSearchComponent> lastSearches = dto.getLastSearches().stream()
                .map(it -> WeatherSearchComponent.fromWeatherSearchDTO(it))
                .collect(Collectors.toList());
        model.addObject(LAST_SEARCHES_ATTRIBUTE, lastSearches);
        model.addObject(WEATHER_INFO_ATTRIBUTE, WeatherInfoComponent.fromWeatherDTO(dto.getWeather()));
    }

}
