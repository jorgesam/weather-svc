package es.jorgesam.weatherservice.controllers.commands;

public class SearchCommand {

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
