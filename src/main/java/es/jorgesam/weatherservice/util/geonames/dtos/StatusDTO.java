package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;

public class StatusDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;
    private int value;

    public StatusDTO() {}

    public StatusDTO(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "StatusDTO [message=" + message + ", value=" + value + "]";
    }

}
