package es.jorgesam.weatherservice.util.geonames.client;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import es.jorgesam.weatherservice.config.GeoNamesConfig;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNameDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.StatusDTO;

/*
 * Wrapper sobre restTemplate para las llamadas al servicio web remoto.
 * 
 * Se logean las peticiones y respuestas a este servicio. Se podría implementar
 * un loggin interceptor y añadirlo al RestTemplateBuilder, pero consumir la
 * response en este interceptor, implicaría que luego no está disponible.
 * 
 * Para ese caso habría que hacer un wrapper de la response de forma que aunque
 * se logee se pueda devolver de nuevo el cuerpo de la petición.
 */
@Component
public class GeoNamesClientImpl implements GeoNamesClient {

    private static final Logger logger = LoggerFactory.getLogger(GeoNamesClientImpl.class);

    public static final String QUERY_PARAM = "query";
    public static final String USERNAME_PARAM = "user";
    public static final String NORTH_PARAM = "north";
    public static final String SOUTH_PARAM = "south";
    public static final String EAST_PARAM = "east";
    public static final String WEST_PARAM = "west";

    private final RestTemplate restTemplate;
    private final GeoNamesConfig config;

    public GeoNamesClientImpl(RestTemplateBuilder restTemplateBuilder, GeoNamesConfig config) {
        this.restTemplate = restTemplateBuilder.rootUri(config.getBaseHost())
                .setConnectTimeout(Duration.ofMillis(config.getConnectionTimeout()))
                .setReadTimeout(Duration.ofMillis(config.getReadTimeout())).build();
        this.config = config;
    }

    @Override
    @Retryable(maxAttempts = 3)
    public GeoNamesSearchResponseDTO searchGeoNames(String query) {
        Map<String, String> params = new HashMap<>();
        params.put(QUERY_PARAM, query);
        params.put(USERNAME_PARAM, this.config.getUser());

        if (logger.isInfoEnabled()) {
            logger.info(String.format("REQUEST: [%s] params %s", config.getGeoInfoQuery(), params));
        }
        GeoNamesSearchResponseDTO dto = restTemplate.getForObject(config.getGeoInfoQuery(),
                GeoNamesSearchResponseDTO.class, params);

        if (logger.isInfoEnabled()) {
            logger.info(String.format("RESPONSE: %s", dto));
        }
        cleanInvalidGeoNames(dto);
        return dto;
    }

    private void cleanInvalidGeoNames(GeoNamesSearchResponseDTO dto) {
        if (dto.getGeonames() != null) {
            List<GeoNameDTO> validGeoNames = dto.getGeonames().stream()
                    .filter(this::validGeoName)
                    .collect(Collectors.toList());
            dto.setGeonames(validGeoNames);
            dto.setTotalResultsCount(validGeoNames.size());
        }
    }

    private boolean validGeoName(GeoNameDTO dto) {
        return (dto.getBbox() != null && dto.getName() != null);
    }

    @Override
    @Retryable(maxAttempts = 3)
    public GeoNamesWeatherResponseDTO searchObservations(GeoNamesSearchResponseDTO geoNames) {
        if (geoNames.getTotalResultsCount() > 0) {
            Map<String, Object> params = new HashMap<>();
            params.put(NORTH_PARAM, geoNames.getNorth());
            params.put(SOUTH_PARAM, geoNames.getSouth());
            params.put(EAST_PARAM, geoNames.getEast());
            params.put(WEST_PARAM, geoNames.getWest());
            params.put(USERNAME_PARAM, this.config.getUser());

            if (logger.isInfoEnabled()) {
                logger.info(String.format("REQUEST: [%s] params %s", config.getWeatherInfoQuery(), params));
            }
            GeoNamesWeatherResponseDTO dto = restTemplate.getForObject(config.getWeatherInfoQuery(), GeoNamesWeatherResponseDTO.class, params);

            if (logger.isInfoEnabled()) {
                logger.info(String.format("RESPONSE: %s", dto));
            }
            return dto;
        } else {
            return null;
        }
    }

    @Recover
    public GeoNamesSearchResponseDTO recoverGeoSearch(Throwable exception, String query) {
        logger.error(String.format("Error en la conexión al servicio remoto: %s", exception.getMessage()));
        GeoNamesSearchResponseDTO errorDto = new GeoNamesSearchResponseDTO();
        errorDto.setStatus(new StatusDTO("Error en la conexión al servicio remoto"));
        return errorDto;
    }

    @Recover GeoNamesWeatherResponseDTO recoverWeatherSearch(Throwable exception, GeoNamesSearchResponseDTO geoNames) {
        logger.error(String.format("Error en la conexión al servicio remoto: %s", exception.getMessage()));
        GeoNamesWeatherResponseDTO errorDto = new GeoNamesWeatherResponseDTO();
        errorDto.setStatus(new StatusDTO("Error en la conexión al servicio remoto"));
        return errorDto;
    }

}
