package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;

public class ObservationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private double lng;
    private double lat;
    private String datetime;
    // Puede estar vacío
    private String temperature;
    private int humidity;
    private String stationName;

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Override
    public String toString() {
        return "ObservationDTO [lng=" + lng + ", lat=" + lat + ", datetime=" + datetime + ", temperature=" + temperature
                + ", humidity=" + humidity + ", stationName=" + stationName + "]";
    }

}
