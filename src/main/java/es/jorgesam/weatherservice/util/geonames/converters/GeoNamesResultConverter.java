package es.jorgesam.weatherservice.util.geonames.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherObservation;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;

@Component
public class GeoNamesResultConverter {

    private static final Logger logger = LoggerFactory.getLogger(GeoNamesResultConverter.class);

    @Value("${geonames.date-pattern}")
    private String datePattern;

    public Optional<Weather> convert(GeoNamesSearchResponseDTO searchDTO,
            GeoNamesWeatherResponseDTO observationsDTO) {

        if (searchDTO != null && observationsDTO != null
                && observationsDTO.getWeatherObservations() != null
                && !observationsDTO.getWeatherObservations().isEmpty()) {

            Weather weather = new Weather();
            weather.setName(searchDTO.getName());
            weather.setCountryCode(searchDTO.getCountryCode());
            weather.setTimezoneId(searchDTO.getTimezoneId());
            weather.setLat(searchDTO.getLat() != null ? Double.valueOf(searchDTO.getLat()) : null);
            weather.setLng(searchDTO.getLng() != null ? Double.valueOf(searchDTO.getLng()) : null);
            
            weather.setObservations(
                    observationsDTO.getWeatherObservations().stream().map(o -> {
                        WeatherObservation observation = new WeatherObservation();
                        observation.setLat(o.getLat());
                        observation.setLng(o.getLng());
                        observation.setStationName(o.getStationName());
                        if (o.getTemperature() != null && o.getTemperature().trim().length() > 0) {
                            observation.setTemperature(Float.valueOf(o.getTemperature()));
                        }
                        observation.setDateTime(parseFromStringOrNull(o.getDatetime()));
                        return observation;
                    }).collect(Collectors.toList()));

            return Optional.of(weather);
        } else {
            return Optional.empty();
        }
    }

    public LocalDateTime parseFromStringOrNull(String date) {
        if (date != null) {
            try {
                return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(datePattern));
            } catch (DateTimeParseException e) {
                logger.warn(String.format("Observation Date [%s] can not be parsed", date));
            }
        }
        return null;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

}
