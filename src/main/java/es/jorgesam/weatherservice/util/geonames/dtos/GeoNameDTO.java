package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;

public class GeoNameDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String countryCode;
    private TimezoneDTO timezone;
    private String asciiName;
    private String lat;
    private String lng;
    private BBoxDTO bbox;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public TimezoneDTO getTimezone() {
        return timezone;
    }

    public void setTimezone(TimezoneDTO timezone) {
        this.timezone = timezone;
    }

    public String getAsciiName() {
        return asciiName;
    }

    public void setAsciiName(String asciiName) {
        this.asciiName = asciiName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public BBoxDTO getBbox() {
        return bbox;
    }

    public void setBbox(BBoxDTO bbox) {
        this.bbox = bbox;
    }

    @Override
    public String toString() {
        return "GeoNameDTO [name=" + name + ", countryCode=" + countryCode + ", timezone=" + timezone + ", asciiName="
                + asciiName + ", lat=" + lat + ", lng=" + lng + ", bbox=" + bbox + "]";
    }

}
