package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;

public class TimezoneDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int gmtOffset;
    private String timeZoneId;
    private int dstOffset;

    public int getGmtOffset() {
        return gmtOffset;
    }

    public void setGmtOffset(int gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public int getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(int dstOffset) {
        this.dstOffset = dstOffset;
    }

    @Override
    public String toString() {
        return "TimezoneDTO [gmtOffset=" + gmtOffset + ", timeZoneId=" + timeZoneId + ", dstOffset=" + dstOffset + "]";
    }

}
