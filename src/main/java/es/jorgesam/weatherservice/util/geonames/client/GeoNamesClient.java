package es.jorgesam.weatherservice.util.geonames.client;

import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;

public interface GeoNamesClient {

    GeoNamesSearchResponseDTO searchGeoNames(String query);

    GeoNamesWeatherResponseDTO searchObservations(GeoNamesSearchResponseDTO geoNames);

}
