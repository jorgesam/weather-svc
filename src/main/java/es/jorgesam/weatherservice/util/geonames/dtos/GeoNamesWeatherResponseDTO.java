package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;
import java.util.Collection;

public class GeoNamesWeatherResponseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Collection<ObservationDTO> weatherObservations;
    private StatusDTO status;

    public Collection<ObservationDTO> getWeatherObservations() {
        return weatherObservations;
    }

    public void setWeatherObservations(Collection<ObservationDTO> weatherObservations) {
        this.weatherObservations = weatherObservations;
    }

    public StatusDTO getStatus() {
        return status;
    }

    public void setStatus(StatusDTO status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GeoNamesWeatherResponseDTO [weatherObservations=" + weatherObservations + ", status=" + status + "]";
    }

}
