package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;
import java.util.List;

public class GeoNamesSearchResponseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int totalResultsCount = 0;
    private List<GeoNameDTO> geonames;
    private StatusDTO status;

    public int getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(int totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }

    public List<GeoNameDTO> getGeonames() {
        return geonames;
    }

    public void setGeonames(List<GeoNameDTO> geonames) {
        this.geonames = geonames;
    }

    public StatusDTO getStatus() {
        return status;
    }

    public void setStatus(StatusDTO status) {
        this.status = status;
    }

    public String getCountryCode() {
        if (this.geonames != null
                && !this.geonames.isEmpty()) {
            return this.geonames.get(0).getCountryCode();
        } else {
            return null;
        }
    }

    public String getTimezoneId() {
        if (this.geonames != null
                && !this.geonames.isEmpty()
                && this.geonames.get(0).getTimezone() != null) {
            return this.geonames.get(0).getTimezone().getTimeZoneId();
        } else {
            return null;
        }
    }

    public Double getNorth() {
        if (this.geonames != null
                && !this.geonames.isEmpty()
                && this.geonames.get(0).getBbox() != null) {
            return this.geonames.get(0).getBbox().getNorth();
        } else {
            return null;
        }
    }

    public Double getSouth() {
        if (this.geonames != null
                && !this.geonames.isEmpty()
                && this.geonames.get(0).getBbox() != null) {
            return this.geonames.get(0).getBbox().getSouth();
        } else {
            return null;
        }
    }

    public Double getEast() {
        if (this.geonames != null
                && !this.geonames.isEmpty()
                && this.geonames.get(0).getBbox() != null) {
            return this.geonames.get(0).getBbox().getEast();
        } else {
            return null;
        }
    }

    public Double getWest() {
        if (this.geonames != null
                && !this.geonames.isEmpty()
                && this.geonames.get(0).getBbox() != null) {
            return this.geonames.get(0).getBbox().getWest();
        } else {
            return null;
        }
    }

    public String getName() {
        if (this.geonames != null
                && !this.geonames.isEmpty()) {
            return this.geonames.get(0).getName();
        } else {
            return null;
        }
    }

    public String getLat() {
        if (this.geonames != null
                && !this.geonames.isEmpty()) {
            return this.geonames.get(0).getLat();
        } else {
            return null;
        }
    }

    public String getLng() {
        if (this.geonames != null
                && !this.geonames.isEmpty()) {
            return this.geonames.get(0).getLng();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "GeoNamesSearchResponseDTO [totalResultsCount=" + totalResultsCount + ", geonames=" + geonames
                + ", status=" + status + "]";
    }


}
