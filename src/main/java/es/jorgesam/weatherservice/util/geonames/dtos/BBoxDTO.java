package es.jorgesam.weatherservice.util.geonames.dtos;

import java.io.Serializable;

public class BBoxDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private double east;
    private double south;
    private double north;
    private double west;
    private int accuracyLevel;

    public BBoxDTO() { }

    public BBoxDTO(double east, double south, double north, double west) {
        super();
        this.east = east;
        this.south = south;
        this.north = north;
        this.west = west;
    }

    public double getEast() {
        return east;
    }

    public void setEast(double east) {
        this.east = east;
    }

    public double getSouth() {
        return south;
    }

    public void setSouth(double south) {
        this.south = south;
    }

    public double getNorth() {
        return north;
    }

    public void setNorth(double north) {
        this.north = north;
    }

    public double getWest() {
        return west;
    }

    public void setWest(double west) {
        this.west = west;
    }

    public int getAccuracyLevel() {
        return accuracyLevel;
    }

    public void setAccuracyLevel(int accuracyLevel) {
        this.accuracyLevel = accuracyLevel;
    }

    @Override
    public String toString() {
        return "BBoxDTO [east=" + east + ", south=" + south + ", north=" + north + ", west=" + west + ", accuracyLevel="
                + accuracyLevel + "]";
    }

}
