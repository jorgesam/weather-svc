package es.jorgesam.weatherservice.util.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * AuthenticationProvider que permite logearnos con cualquier usuario no vacío.
 *
 */
public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) {
        //Nothing to do
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) {
        if (username != null && !username.trim().isEmpty()) {
            return User.withUsername(username)
                    .password("dummy").authorities("ROLE_USER").build();
        } else {
            throw new UsernameNotFoundException("Nombre de usuario vacío no válido");
        }
    }

}
