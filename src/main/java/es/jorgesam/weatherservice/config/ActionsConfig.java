package es.jorgesam.weatherservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import es.jorgesam.weatherservice.core.actions.weather.GetWeatherSearchesAction;
import es.jorgesam.weatherservice.core.actions.weather.SaveWeatherSearchAction;
import es.jorgesam.weatherservice.core.actions.weather.SearchWeatherAction;
import es.jorgesam.weatherservice.core.domain.WeatherRepository;
import es.jorgesam.weatherservice.core.domain.WeatherSearchRepository;

/*
 * Ya que las acciones del modelo son lógica de negocio que no dependen
 * de ninguna implementación en concreto, no se han creado como interfaces.
 * 
 * Si es posible que alguna acción pueda tener estrategias distintas
 * se podrían cambiar a interfaces y elegir la implementación a usar, ya
 * sea de modo general o mediante un patrón estrategia.
 * 
 * Para que las acciones del modelo no tengan constancia de spring, se
 * configuran como beans en este fichero. 
 */
/**
 * Configuración de las acciones del modelo
 *
 */
@Configuration
public class ActionsConfig {

    @Bean
    public SearchWeatherAction searchWeatherAction(WeatherRepository weatherRepository,
            SaveWeatherSearchAction saveWeatherSearchAction) {
        return new SearchWeatherAction(weatherRepository, saveWeatherSearchAction);
    }

    @Bean
    public SaveWeatherSearchAction saveWeatherAction(
            WeatherSearchRepository weatherSearchRepository) {
        return new SaveWeatherSearchAction(weatherSearchRepository);
    }

    @Bean
    public GetWeatherSearchesAction getWeatherSearchesAction(
            WeatherSearchRepository weatherSearchRepository) {
        return new GetWeatherSearchesAction(weatherSearchRepository);
    }
}
