package es.jorgesam.weatherservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Mapea la configuración bajo la clave geonames en los ficheros de configuración.
 *
 */
@Configuration
@ConfigurationProperties(prefix = "geonames")
public class GeoNamesConfig {

    private String baseHost;
    private String user;
    private String geoInfoQuery;
    private String weatherInfoQuery;
    private long connectionTimeout;
    private long readTimeout;
    private String datePattern;

    public String getBaseHost() {
        return baseHost;
    }

    public void setBaseHost(String baseHost) {
        this.baseHost = baseHost;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getGeoInfoQuery() {
        return geoInfoQuery;
    }

    public void setGeoInfoQuery(String geoInfoQuery) {
        this.geoInfoQuery = geoInfoQuery;
    }

    public String getWeatherInfoQuery() {
        return weatherInfoQuery;
    }

    public void setWeatherInfoQuery(String weatherInfoQuery) {
        this.weatherInfoQuery = weatherInfoQuery;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

}
