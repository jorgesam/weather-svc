package es.jorgesam.weatherservice.core.domain;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.util.geonames.client.GeoNamesClient;
import es.jorgesam.weatherservice.util.geonames.converters.GeoNamesResultConverter;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesSearchResponseDTO;
import es.jorgesam.weatherservice.util.geonames.dtos.GeoNamesWeatherResponseDTO;

/*
 * Implementación del repositorio weatherRepositorio que usará las clases de utilidad
 * GeoNamesClient para realizar la llamada final y GeoNamesResultConverter para pasar los
 * DTOs devueltos a entidades de dominio.
 * 
 * Separar la llamada a los servicios web en otra clase, nos permite poder anotar sus métodos con
 * @Retry para que reintente en caso de fallo
 * 
 */
@Repository
public class WeatherRepositoryImpl implements WeatherRepository {

    private final GeoNamesClient client;
    private final GeoNamesResultConverter converter;

    public WeatherRepositoryImpl(GeoNamesClient geoNamesClient, GeoNamesResultConverter converter) {
        this.client = geoNamesClient;
        this.converter = converter;
    }

    @Override
    public Optional<Weather> findBy(String query) throws DataAccessException {
        Optional<Weather> returnValue = Optional.empty();

        if (query != null && query.trim().length() > 0) {
            GeoNamesSearchResponseDTO searchResponse = client.searchGeoNames(query);
            checkErrorResponse(searchResponse);
            GeoNamesWeatherResponseDTO weatherResponse = client.searchObservations(searchResponse);
            checkErrorResponse(weatherResponse);
            if (weatherResponse != null && weatherResponse.getWeatherObservations() != null
                    && !weatherResponse.getWeatherObservations().isEmpty()) {

                returnValue = converter.convert(searchResponse, weatherResponse);
            }
        }
        return returnValue;
    }

    private void checkErrorResponse(GeoNamesWeatherResponseDTO weatherResponse) throws DataAccessException {
        if (weatherResponse != null && weatherResponse.getStatus() != null) {
            throw new DataAccessException(weatherResponse.getStatus().getMessage());
        }
    }

    private void checkErrorResponse(GeoNamesSearchResponseDTO searchResponse) throws DataAccessException {
        if (searchResponse != null && searchResponse.getStatus() != null) {
            throw new DataAccessException(searchResponse.getStatus().getMessage());
        }
    }

}
