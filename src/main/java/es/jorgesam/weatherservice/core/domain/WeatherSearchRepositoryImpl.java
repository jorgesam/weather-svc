package es.jorgesam.weatherservice.core.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/*
 * Implementación de la interfaz del repositorio, haciendo uso de spring-data
 */
public interface WeatherSearchRepositoryImpl extends WeatherSearchRepository, CrudRepository<WeatherSearch, Long> {

    @Override
    List<WeatherSearch> findTop10ByUsernameOrderByIdDesc(String username);
}
