package es.jorgesam.weatherservice.core.domain;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class WeatherSearch {

    @Id
    @GeneratedValue
    private Long id;
    private String query;
    private String username;
    private Instant timestamp;
    private String name; //Name returned on Web Service. Used to display the original name

    public WeatherSearch() {}

    public WeatherSearch(String query, String username, Instant timestamp, String name) {
        super();
        this.query = query;
        this.username = username;
        this.timestamp = timestamp;
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
