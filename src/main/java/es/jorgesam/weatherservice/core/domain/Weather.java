package es.jorgesam.weatherservice.core.domain;

import java.util.Collection;

public class Weather {

    private String name;
    private String countryCode;
    private String timezoneId;
    private Double lat;
    private Double lng;
    private Collection<WeatherObservation> observations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = timezoneId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Collection<WeatherObservation> getObservations() {
        return observations;
    }

    public void setObservations(Collection<WeatherObservation> observations) {
        this.observations = observations;
    }

    public Float getTemperatureAvg() {
        int numberOfValidObservations = 0;
        float sumOfObservation = 0f;
        if (observations != null) {
            for (WeatherObservation o : this.observations) {
                if (o.getTemperature() != null) {
                    sumOfObservation += o.getTemperature().floatValue();
                    numberOfValidObservations++;
                }
            }
        }
        if (numberOfValidObservations > 0) {
            return sumOfObservation / numberOfValidObservations;
        } else {
            return null;
        }
    }

}
