package es.jorgesam.weatherservice.core.domain;

import java.util.List;

/*
 * Esta interfaz podríamos saltárnosla, se ha creado para que la interfaz del repositorio
 * no dependa de spring-data directamente, pero normalmente se usa la interfaz tal
 * como la WeatherSearchRepositoryImpl 
 */
public interface WeatherSearchRepository {

    WeatherSearch save(WeatherSearch weatherSearch);

    List<WeatherSearch> findTop10ByUsernameOrderByIdDesc(String username);
}
