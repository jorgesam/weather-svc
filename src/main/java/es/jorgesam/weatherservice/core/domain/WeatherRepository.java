package es.jorgesam.weatherservice.core.domain;

import java.util.Optional;

import es.jorgesam.weatherservice.core.exceptions.DataAccessException;

public interface WeatherRepository {

    Optional<Weather> findBy(String query) throws DataAccessException;

}
