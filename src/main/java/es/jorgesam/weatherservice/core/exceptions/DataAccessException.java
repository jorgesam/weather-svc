package es.jorgesam.weatherservice.core.exceptions;

public class DataAccessException extends Exception {

    public DataAccessException(String message) {
        super(message);
    }

}
