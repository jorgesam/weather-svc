package es.jorgesam.weatherservice.core.actions.weather;

import java.util.ArrayList;
import java.util.List;

import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.domain.WeatherSearchRepository;

/**
 * Acciones para recuperar datos de búsquedas realizadas
 *
 */
public class GetWeatherSearchesAction {

    private final WeatherSearchRepository repository;

    public GetWeatherSearchesAction(WeatherSearchRepository repository) {
        super();
        this.repository = repository;
    }

    /**
     * Recupear las últimas 10 búsquedas por usuario
     * 
     * @param username
     * @return
     */
    public List<WeatherSearch> findTop10ByUser(String username) {
        if (username != null && username.trim().length() > 0) {
            return repository.findTop10ByUsernameOrderByIdDesc(username);
        } else {
            return new ArrayList<>();
        }
    }

}
