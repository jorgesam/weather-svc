package es.jorgesam.weatherservice.core.actions.weather;

import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.domain.WeatherSearchRepository;

/**
 * Acción para salvar una búsqueda realizada usando el repositorio
 * (desconocemos la implementación en esta clase)
 */
public class SaveWeatherSearchAction {

    private final WeatherSearchRepository repository;

    public SaveWeatherSearchAction(WeatherSearchRepository repository) {
        super();
        this.repository = repository;
    }

    public WeatherSearch save(WeatherSearch weather) {
        return repository.save(weather);
    }

}
