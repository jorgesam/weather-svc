package es.jorgesam.weatherservice.core.actions.weather;

import java.time.Instant;
import java.util.Optional;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherRepository;
import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.exceptions.DataAccessException;

/**
 * Realiza la búsqueda de datos meteorológicos en la localización dada.
 * Si se encuentran resultados, se guarda como búsqueda realizada.
 */
public class SearchWeatherAction {

    private WeatherRepository weatherRepository;
    private SaveWeatherSearchAction saveWeatherSearchAction;

    public SearchWeatherAction(WeatherRepository weatherRepository, SaveWeatherSearchAction saveWeatherSearchAction) {
        this.weatherRepository = weatherRepository;
        this.saveWeatherSearchAction = saveWeatherSearchAction;
    }

    public Optional<Weather> findBy(String query, String username, Instant timestamp) throws DataAccessException {
        if (null == username || username.trim().length() == 0) {
            throw new IllegalArgumentException("username can not be null or empty");
        } else if (timestamp == null) {
            throw new IllegalArgumentException("timestamp can not be null");
        }

        Optional<Weather> weather = weatherRepository.findBy(query);
        if (weather.isPresent()) {
            saveWeatherSearchAction.save(new WeatherSearch(query, username, timestamp, weather.get().getName()));
        }
        return weather;
    }
}
