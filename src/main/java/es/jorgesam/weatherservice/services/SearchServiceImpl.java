package es.jorgesam.weatherservice.services;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.jorgesam.weatherservice.core.actions.weather.GetWeatherSearchesAction;
import es.jorgesam.weatherservice.core.actions.weather.SearchWeatherAction;
import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherSearch;
import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.services.dto.WeatherAndLastSearchesDTO;
import es.jorgesam.weatherservice.services.dto.WeatherDTO;
import es.jorgesam.weatherservice.services.dto.WeatherSearchDTO;

@Service
public class SearchServiceImpl implements SearchService {

    private SearchWeatherAction searchWeatherAction;
    private GetWeatherSearchesAction getWeatherSearchesAction;

    @Autowired
    public SearchServiceImpl(SearchWeatherAction searchWeatherAction,
            GetWeatherSearchesAction getWeatherSearchesAction) {
        super();
        this.searchWeatherAction = searchWeatherAction;
        this.getWeatherSearchesAction = getWeatherSearchesAction;
    }

    @Override
    @Transactional
    public WeatherAndLastSearchesDTO search(String query, String user, Instant timestamp) throws DataAccessException {
        Optional<Weather> weather = searchWeatherAction.findBy(query, user, timestamp);
        List<WeatherSearch> lastSearches = getWeatherSearchesAction.findTop10ByUser(user);
        return new WeatherAndLastSearchesDTO(
                weather.map(w -> WeatherDTO.from(w)),
                lastSearches.stream()
                    .map(w -> WeatherSearchDTO.from(w))
                    .collect(Collectors.toList()));
    }

}
