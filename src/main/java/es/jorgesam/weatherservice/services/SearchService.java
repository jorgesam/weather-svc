package es.jorgesam.weatherservice.services;

import java.time.Instant;

import es.jorgesam.weatherservice.core.exceptions.DataAccessException;
import es.jorgesam.weatherservice.services.dto.WeatherAndLastSearchesDTO;

/**
 * Servicio de búsquedas meteorológicas de una localización
 *
 */
public interface SearchService {

    /**
     * Implementa caso de uso de búsqueda y obtener las últimas búsquedas del mismo usuario
     * 
     * @param query
     * @param user
     * @param timestamp
     * @return
     * @throws DataAccessException
     */
    /*
     * Los parámetros user y timestamp podríamos obtenerlos dentro del modelo, pero esto
     * significaría acceder al contexto de spring desde el modelo de negocio en el caso
     * del usuario, y no permitir guardar la búsqueda como si fuese hecha en otro momento (
     * (por ejemplo carga de históricos) en el caso del timestamp.
     * 
     * Si alguno de los 2 fuese una condición fuerte en lógica de negocio, deberían
     * estar encapsulados dentro del servicio.
     */
    WeatherAndLastSearchesDTO search(String query, String user, Instant timestamp) throws DataAccessException;

}
