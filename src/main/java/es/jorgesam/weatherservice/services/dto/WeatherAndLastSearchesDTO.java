package es.jorgesam.weatherservice.services.dto;

import java.util.List;
import java.util.Optional;

public class WeatherAndLastSearchesDTO {

    private Optional<WeatherDTO> weather;
    private List<WeatherSearchDTO> lastSearches;

    public WeatherAndLastSearchesDTO(Optional<WeatherDTO> weather, List<WeatherSearchDTO> lastSearches) {
        super();
        this.weather = weather;
        this.lastSearches = lastSearches;
    }

    public Optional<WeatherDTO> getWeather() {
        return weather;
    }

    public List<WeatherSearchDTO> getLastSearches() {
        return lastSearches;
    }

    @Override
    public String toString() {
        return "WeatherAndLastSearchesDTO [weather=" + weather + ", lastSearches=" + lastSearches + "]";
    }

}
