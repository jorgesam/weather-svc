package es.jorgesam.weatherservice.services.dto;

import es.jorgesam.weatherservice.core.domain.WeatherObservation;

public class WeatherObservationDTO {

    private String stationName;
    private double lat;
    private double lng;
    private Float temperature;

    public static WeatherObservationDTO from(WeatherObservation model) {
        WeatherObservationDTO dto = new WeatherObservationDTO();
        dto.stationName = model.getStationName();
        dto.lat = model.getLat();
        dto.lng = model.getLng();
        dto.temperature = model.getTemperature();
        return dto;
    }

    public String getStationName() {
        return stationName;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public Float getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "WeatherObservationDTO [stationName=" + stationName + ", lat=" + lat + ", lng=" + lng + ", temperature="
                + temperature + "]";
    }

}
