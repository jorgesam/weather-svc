package es.jorgesam.weatherservice.services.dto;

import java.util.ArrayList;
import java.util.Collection;

import es.jorgesam.weatherservice.core.domain.Weather;
import es.jorgesam.weatherservice.core.domain.WeatherObservation;

public class WeatherDTO {

    private String name;
    private String countryCode;
    private String timezoneId;
    private Double lat;
    private Double lng;
    private Float temperature;
    private Collection<WeatherObservationDTO> observations;

    public static WeatherDTO from(Weather model) {
        WeatherDTO dto = new WeatherDTO();
        dto.name = model.getName();
        dto.countryCode = model.getCountryCode();
        dto.timezoneId = model.getTimezoneId();
        dto.lat = model.getLat();
        dto.lng = model.getLng();
        dto.temperature = model.getTemperatureAvg();
        Collection<WeatherObservationDTO> observations = new ArrayList<>();
        if (model.getObservations() != null) {
            for (WeatherObservation observation : model.getObservations()) {
                observations.add(WeatherObservationDTO.from(observation));
            }
        }
        dto.observations = observations;
        return dto;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public Collection<WeatherObservationDTO> getObservations() {
        return observations;
    }

    public Float getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "WeatherDTO [name=" + name + ", countryCode=" + countryCode + ", timezoneId=" + timezoneId + ", lat="
                + lat + ", lng=" + lng + ", temperature=" + temperature + ", observations=" + observations + "]";
    }

}
