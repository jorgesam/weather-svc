package es.jorgesam.weatherservice.services.dto;

import es.jorgesam.weatherservice.core.domain.WeatherSearch;

public class WeatherSearchDTO {

    private String name;

    public static WeatherSearchDTO from(WeatherSearch model) {
        WeatherSearchDTO dto = new WeatherSearchDTO();
        dto.name = model.getName();
        return dto;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "WeatherSearchDTO [name=" + name + "]";
    }

}
