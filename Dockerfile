FROM openjdk:11-jre-slim

# Instalamos curl para usarlo en el healthcheck
RUN apt update -y \
    && apt install -y curl \
    && apt-get clean && rm -r /var/lib/apt/lists/*

EXPOSE 8080

HEALTHCHECK --start-period=10s \
    CMD curl -f http://localhost:8080/actuator/health || exit 1

# Valores por defecto de JAVA_OPTS, se podrán sobreescribir en el lanzamiento del contenedor
ENV JAVA_OPTS="-XX:+UseG1GC -Dsun.net.inetaddr.ttl=60"

# Comando que se ejecutará al arranque del contenedor
CMD java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /application.jar

# Como última capa (la que mas va a cambiar), añadimos la aplicación a la imagen.
COPY target/weather-service.jar /application.jar
