# weather-service

Realiza la búsqueda de una localización y muestra la temperatura existente, así como la localización de las estaciones usadas.

## Compilación y empaquetado

**Necesario**: maven 3 y Java 11

    mvn clean verify

Si queremos crear la imagen docker, después de crear el empaquetado

    mvn dockerfile:build

_Nota: la imágen está disponible en docker hub para su descarga: jorgesam/weather-service_

## Ejecución

Podemos lanzarla mediante java:

    java -jar target/weather-service.jar

ó con docker

    docker run --rm -d -p 8080:8080 --name weather-service jorgesam/weather-service

_Nota: En la url http://ec2-52-17-237-69.eu-west-1.compute.amazonaws.com se ha desplegado esta imagen, con lo que también se puede probar aquí_

## Configuración en ejecución

Se pueden cambiar los valores de configuración en el momento del lanzamiento, para eso usamos:

    java -jar target/weather-service.jar --geonames.user=anotheruser

Ó mediante docker:

    docker run --rm -d -p 8080:8080 --name weather-service -e GEONAMES_USER=anotheruser jorgesam/weather-service

# Estructura del código

Carpeta src/main/java:

* es/jorgesam/weatherservice/core/domain. Entidades del dominio y repositorios para acceder a ellas.
    * Los repositorios tiene una interfaz que no está asociada a ninguna tecnología
    * La implementación ya usa spring-data o llama al cliente de rest para recuperar las entidades remotas.
      Se podrían separar a un módulo aparte.

* es/jorgesam/weatherservice/core/actions. Acciones / casos de uso
    * Realizan su trabajo en base a las entidades y los repositorios.
    * Nos permite encapsular los casos de uso y que la capa de servicios quede mas limpia
    * Ya que no contienen ninguna referencia a implementación, no se han creado interfaces.

* es/jorgesam/weatherservice/services. Capa de servicios que ofrece el modelo de negocio. Contiene:
    * Interfaz de los servicios
    * Implementación de los servicios
    * DTOs para la comunicación de los datos con capas superiores.
      De esta forma, las capas que usen los servicios no están acopladas al modelo de negocio

* es/jorgesam/weatherservice/controllers. Capa de controladores para la interfaz web.
    * Hace uso de los servicios que se proveen
    * Puede realizar validaciones de datos de entrada o formateos de datos de salida, pero no debe implementar lógica de negocio

* es/jorgesam/weatherservice/controllers/components. Componentes reutilizables para la interfaz.
    * Están relacionados con las plantillas thymeleaf en src/main/resources/components
    * Permite que el mismo componente pueda ser usado en distintos sitios de la aplicación web.
      El controlador rellena los datos en el objeto que necesita la plantilla y así lo abstrae de donde se use.

* es/jorgesam/weatherservice/utils/geonames. Clases de utilidad para la comunicación y recuperación de datos del servicio de geonames
    * En `client` están encapsuladas las llamadas con rest template, de forma que usamos spring-retry
      para reintentar la petición en caso de un fallo de red.

* es/jorgesam/weatherservice/utils/security. Contiene una implementación personalizada del authentication provider
  que nos permite logearnos con cualqueir usuario y contraseña sin verificación alguna.

* es/jorgesam/weatherservice/config. Diversas clases de configuración que realizarán la inyección de dependencias
  y configurarán diversos servicios
    * ActionsConfig. Instancia los beans de los casos de uso, de esta forma no necesitamos añadir anotaciones en los ficheros de acciones
    * GeoNamesConfig. Clase java que se mapea con la configuración en el fichero application.yml bajo la clave `geonames`.
    * SecurityConfig. Configura la seguridad web y nuestro authentication provider

Carpeta src/main/resources:

* En la carpeta `templates` tenemos las plantillas de thymeleaf que usarán los controladores como vista. Dentro de esta tenemos:
    * Carpeta componentes que serán las visatas relacionadas con los componentes del controlador
    * Carpeta de vistas de error. Se mapean por códigos de error, así si se produce un error 404 se usa la vista error/404.html

Carpeta src/test/java

* Diversos tests unitarios que se han realizado en el desarrollo de la aplicación
* Contiene un test de integración, que es el que trae spring boot por defecto. Comprueba que el contexto cargue (nos permite probar que la inyección de dependencias está correcta). No se han desarrollado mas tests de integración

# Comentarios sobre el desarrollo

* Para facilitar el lanzamiento de la aplicación se está usando una base de datos embebida en memoria.
  Cuando el servidor se apaga se pierden los datos. Lo suyo en una aplicación real, sería cambiarlo por una base de datos externa.

* Para poder cargar el código en un IDE, no se ha hecho uso de lombok, ya que para usarlo en un IDE
  es necesario instalar manualmente una libreria. El proyecto lombok permite agilizar el desarrollo de POJOs

* Las inyecciones de dependencias se hacen en los constructores y no usando @Autowired en un atributo.
  Esto nos permite una inyección de dependencias mas clara cuando lo hacemos por configuración y poder realizar pruebas
  más facilmente, al poder pasarle al constructor otra implementación.

* Las páginas de login y logout, son las implementadas por defecto por spring boot, no se han personalizado.

* Se ha añadido también la librería actuator que añade funcionalidades interesante preparadas para una aplicación en producción.
  Se usa el healtcheck que provee para la imagen docker.

* Mejoras que estaría bien implementar:
    * Uso de una caché para almacenar los resultados de las consultas hechas al servicio web externo.
      Al menos el de posiciones geográficas no debería cambiar
    * Esta caché se podría usar también para devolver datos si la llamada al servicio web falla (aunque reintentemos). Habría que indicar
      que los datos se están recuperando desde una caché
